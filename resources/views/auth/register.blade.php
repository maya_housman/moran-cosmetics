@extends('layouts.moran')

@section('content')
    <div class="card-header">{{ __('Register') }}</div>

    <div class="card-body">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="field half first">
                <label for="name">{{ __('Name') }}</label>

                <input id="name"
                       type="text"
                       class="@error('name') is-invalid @enderror"
                       name="name"
                       value="{{ old('name') }}"
                       required
                       autocomplete="name"
                       autofocus>

                @error('name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="field half">
                <label for="email">{{ __('E-Mail Address') }}</label>

                <input id="email"
                       type="email"
                       class="@error('email') is-invalid @enderror"
                       name="email"
                       value="{{ old('email') }}"
                       required
                       autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="field half first">

                <label for="password">{{ __('Password') }}</label>

                <input id="password"
                       type="password"
                       class="@error('password') is-invalid @enderror"
                       name="password"
                       required
                       autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="field half">
                <label for="password-confirm"
                       class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <input id="password-confirm"
                       type="password"
                       name="password_confirmation"
                       required
                       autocomplete="new-password">
            </div>

            <div class="field half first">

                <label for="phone">{{ __('Phone') }}</label>

                <input id="phone"
                       type="text"
                       class="@error('phone') is-invalid @enderror"
                       name="phone"
                       required
                       autocomplete="new-phone">

                @error('phone')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
