<!DOCTYPE HTML>
<html>
<head>
    <title>Introspect by TEMPLATED</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/jquery.min.js') }}" defer></script>
    <link rel="stylesheet" href={{ asset('assets/css/main.css') }} />
</head>
<body>

<!-- Header -->
<header id="header">
    <div class="inner">
        <a href="{{ url('/appointments') }}" class="logo">Moran Cosmetics</a>
        <nav id="nav">
            @auth
                @if(auth()->user()->role->name === 'admin')
                    <a href="{{ url('/appointments') }}">
                        Appointments
                    </a>
                @else
                    <a href="{{ url('/appointments') }}">
                        My Appointments
                    </a>
                @endif
                <a href="{{ url('/treatments') }}">
                    Treatments
                </a>
            @endauth
            <a href="{{ url('/appointments/create') }}">
                Add new appointment
            </a>
            @guest
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                @if (Route::has('register'))
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                @endif
            @else
                <span>{{ Auth::user()->name }}</span>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            @endguest

        </nav>
    </div>
</header>
<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- One -->
<section id="one">
    <div class="inner">
        @yield('content')
    </div>
</section>

<!-- Scripts -->
<script src="{{ asset('assets/js/jquery.min.js') }}" defer></script>
<script src="{{ asset('assets/js/skel.min.js') }}" defer></script>
<script src="{{ asset('assets/js/util.js') }}" defer></script>
<script src="{{ asset('assets/js/main.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function () {


		$('#master').on('click', function(e) {
			if($(this).is(':checked',true))
			{
				$(".sub_chk").prop('checked', true);
			} else {
				$(".sub_chk").prop('checked',false);
			}
		});


		$('.delete_all').on('click', function(e) {


			var allVals = [];
			$(".sub_chk:checked").each(function() {
				allVals.push($(this).attr('data-id'));
			});


			if(allVals.length <=0)
			{
				alert("Please select row.");
			}  else {


				var check = confirm("Are you sure you want to delete this row?");
				if(check == true){


					var join_selected_values = allVals.join(",");


					$.ajax({
						url: $(this).data('url'),
						type: 'DELETE',
						headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
						data: 'ids='+join_selected_values,
						success: function (data) {
							if (data['success']) {
								$(".sub_chk:checked").each(function() {
									$(this).parents("tr").remove();
								});
								alert(data['success']);
							} else if (data['error']) {
								alert(data['error']);
							} else {
								alert('Whoops Something went wrong!!');
							}
						},
						error: function (data) {
							alert(data.responseText);
						}
					});


					$.each(allVals, function( index, value ) {
						$('table tr').filter("[data-row-id='" + value + "']").remove();
					});
				}
			}
		});


		$('[data-toggle=confirmation]').confirmation({
			rootSelector: '[data-toggle=confirmation]',
			onConfirm: function (event, element) {
				element.trigger('confirm');
			}
		});


		$(document).on('confirm', function (e) {
			var ele = e.target;
			e.preventDefault();


			$.ajax({
				url: ele.href,
				type: 'DELETE',
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function (data) {
					if (data['success']) {
						$("#" + data['tr']).slideUp("slow");
						alert(data['success']);
					} else if (data['error']) {
						alert(data['error']);
					} else {
						alert('Whoops Something went wrong!!');
					}
				},
				error: function (data) {
					alert(data.responseText);
				}
			});


			return false;
		});
	});
</script>

</body>
</html>