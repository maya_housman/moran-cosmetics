@extends('layouts.moran')

@section('title', 'Appointments')

@section('content')

    <h1>Appointments</h1>
    <table>
        <tr>
            @if(auth()->user()->role->name === 'admin')
                <th>
                    <input class="form-check-input"
                           type="checkbox"
                           id="master">
                    <label class="form-check-label" for="master"></label>
                </th>
                <th>@sortablelink('user_id', 'User Name')</th>
                <th>Phone</th>
            @else
                <th>Treatment Cost</th>
            @endif
            <th>@sortablelink('payment_status_id', 'Payment Status')</th>
            <th>@sortablelink('treatment_id', 'Treatment')</th>
            <th>@sortablelink('start_time','Date')</th>
            <th>Time</th>
            <th>@sortablelink('appointment_status_id','Status')</th>
            <th></th>
            @if(auth()->user()->role->name !== 'admin')
                <th></th>
            @endif
        </tr>
        <!-- the table data -->
        @foreach($appointments as $appointment)
            <tr>
                @if(auth()->user()->role->name === 'admin')
                    <td>
                        <input class="form-check-input sub_chk"
                               type="checkbox"
                               name="check-{{$appointment->id}}"
                               data-id="{{$appointment->id}}"
                               id="check-{{$appointment->id}}">
                        <label class="form-check-label" for="check-{{$appointment->id}}"></label>
                    </td>
                    <td>{{$appointment->user->name}}</td>
                    <td>{{$appointment->user->phone}}</td>
                @else
                    <td>{{$appointment->treatment->cost}}</td>
                @endif
                <td>{{$appointment->paymentStatus->name}}</td>
                <td>{{$appointment->treatment->name}}</td>
                <td>{{$appointment->date}}</td>
                <td>{{$appointment->time}}</td>
                <td>{{$appointment->appointmentStatus->name}}</td>
                @if(auth()->user()->role->name === 'admin')
                    <td>
                        <a href="{{route('appointments.edit',$appointment->id)}}">Edit</a>
                    </td>
                        <td>
                            <a href="{{route('appointment.delete',$appointment->id)}}">Delete</a>
                        </td>
                @elseif(\Carbon\Carbon::make($appointment->start_time) > \Carbon\Carbon::now()->addDay())
                    <td>
                        <a href="{{route('appointments.edit',$appointment->id)}}">Edit</a>
                    </td>
                    <td>
                        <a href="{{route('appointment.delete',$appointment->id)}}">Delete</a>
                    </td>
                @endif

            </tr>
        @endforeach

    </table>
    @if(auth()->user()->role->name === 'admin')
        <button class="delete_all" data-url="{{ url('appointments-bulk-delete') }}">Delete All
            Selected
        </button>
    @endif
    {{ $appointments->withQueryString()->links() }}
@endsection