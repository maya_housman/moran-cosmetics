@extends('layouts.moran')

@section('title', 'Create Appointment')

@section('content')
    <h1>Create Appointment</h1>
    <form method="post" action="{{action('AppointmentController@store')}}">
        @csrf
        @if(auth()->user()->role->name === 'admin')
            <div class="form-group">
                <label for="user_id">Customer</label>
                <select class="form-control" name="user_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div>
            <label for="treatment_id">Treatment</label>
            <select class="form-control" name="treatment_id">
                @foreach($treatments as $treatment)
                    <option value="{{$treatment->id}}">{{$treatment->name}}, Price: {{$treatment->cost}}, Duration: {{$treatment->duration}} Minutes</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="start_time">Date and Time</label>
            <div>Please book appointment between Sunday-Thursday, 8AM-17PM, Only round hours, and start in more then 24 hours</div>
            <input type="datetime-local" class="form-control" name="start_time">
            @error('start_time')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            @error('round_hour')
            <br>
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            @error('start_day')
            <br>
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            @error('appointment_collisions')
            <br>
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            @error('start_after_day')
            <br>
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div>
            <input type="submit" name="submit" value="Create appointment">
        </div>
    </form>

@endsection