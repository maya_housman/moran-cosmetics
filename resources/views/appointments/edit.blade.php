@extends('layouts.moran')

@section('title', 'Edit Appointment')

@section('content')
    <h1>Edit Appointment</h1>
    <form method="post" action="{{action('AppointmentController@update', $appointment->id)}}">
        @csrf
        @METHOD('PATCH')
        @if(auth()->user()->role->name === 'admin')
            <div class="form-group">
                <label for="user_id">Customer</label>
                <select class="form-control" name="user_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}" {{$user->id === $appointment->user_id ? 'selected' : ''}}>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="appointment_status_id">Appointment status</label>
                    @if (null != App\AppointmentStatus::next($appointment->appointment_status_id))
                        <div class="select-wrapper">
                            <select name="appointment_status_id" id="appointment_status_id">
                                @foreach(App\AppointmentStatus::next($appointment->appointment_status_id) as $appointment_status)
                                    <option value="{{$appointment_status->id}}" {{$appointment_status->id === $appointment->appointment_status_id ? 'selected' : ''}}>{{$appointment_status->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <input value="{{$appointment->appointmentStatus->id}}"  hidden type="number" class="form-control" name="appointment_status_id">
                        <input value="{{$appointment->appointmentStatus->name}}" disabled>
                    @endif
            </div>
            <div class="form-group">
                <label for="payment_status_id">Payment Status</label>
                <select class="form-control" name="payment_status_id">
                    @foreach($payment_statuses as $payment_status)
                        <option value="{{$payment_status->id}}" {{$payment_status->id === $appointment->payment_status_id ? 'selected' : ''}}>{{$payment_status->name}}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div>
            <label for="treatment_id">Treatment</label>
            <select class="form-control" name="treatment_id">
                @foreach($treatments as $treatment)
                    <option value="{{$treatment->id}}" {{$treatment->id === $appointment->treatment_id ? 'selected' : ''}}>{{$treatment->name}}
                        , Price: {{$treatment->cost}}, Duration: {{$treatment->duration}} Minutes
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="start_time">Date and Time</label>
            <input value="{{\Carbon\Carbon::make($appointment->start_time)->toDateTimeLocalString()}}"
                   type="datetime-local"
                   class="form-control"
                   name="start_time">
        </div>
        <div>
            <input type="submit" name="submit" value="Update appointment">
        </div>
    </form>

@endsection