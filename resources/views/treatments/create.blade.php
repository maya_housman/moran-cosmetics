@extends('layouts.moran')

@section('title', 'Create Treatment')

@section('content')
    <h1>Create Treatment</h1>
    <form method="post" action="{{action('TreatmentController@store')}}">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="cost">Cost</label>
            <input type="number" class="form-control" name="cost">
        </div>
        <div class="form-group">
            <label for="duration">Duration</label>
            <input type="number" class="form-control" name="duration">
        </div>
        <div>
            <input type="submit" name="submit" value="Create treatment">
        </div>
    </form>

@endsection