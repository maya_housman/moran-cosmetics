@extends('layouts.moran')

@section('title', 'Edit Treatment')

@section('content')
    <h1>Edit Treatment</h1>
    <form method="post" action="{{action('TreatmentController@update', $treatment->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for="name">Name</label>
            <input value="{{$treatment->name}}" type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="cost">Cost</label>
            <input value="{{$treatment->cost}}" type="number" class="form-control" name="cost">
        </div>
        <div class="form-group">
            <label for="duration">Duration</label>
            <input value="{{$treatment->duration}}" type="number" class="form-control" name="duration">
        </div>
        <div>
            <input type="submit" name="submit" value="Update treatment">
        </div>
    </form>

@endsection