@extends('layouts.moran')

@section('title', 'Treatments')

@section('content')

    <h1>Treatments</h1>
    @if(auth()->user()->role->name === 'admin')
        <div class="row">
            <a href="{{url('/treatments/create')}}" class="btn btn-info">Add new treatment</a>
        </div>
    @endif
    <table>
        <tr>
            @if(auth()->user()->role->name === 'admin')
                <th>
                    <input class="form-check-input"
                           type="checkbox"
                           id="master">
                    <label class="form-check-label" for="master"></label>
                </th>
            @endif
            <th>@sortablelink('name', 'Tratment Name')</th>
            <th>@sortablelink('cost')</th>
            <th>@sortablelink('duration')</th>
            <th></th>
        </tr>
        <!-- the table data -->
        @foreach($treatments as $treatment)
            <tr>
                @if(auth()->user()->role->name === 'admin')
                    <td>
                        <input class="form-check-input sub_chk"
                               type="checkbox"
                               name="check-{{$treatment->id}}"
                               data-id="{{$treatment->id}}"
                               id="check-{{$treatment->id}}">
                        <label class="form-check-label" for="check-{{$treatment->id}}"></label>
                    </td>
                @endif
                <td>{{$treatment->name}}</td>
                <td>{{$treatment->cost}}</td>
                <td>{{$treatment->duration}}</td>
                @if(auth()->user()->role->name === 'admin')
                    <td>
                        <a href="{{route('treatments.edit',$treatment->id)}}">Edit</a>
                    </td>
                @endif
            </tr>
        @endforeach

    </table>
    @if(auth()->user()->role->name === 'admin')
        <button class="delete_all" data-url="{{ url('treatments-bulk-delete') }}">Delete All
            Selected
        </button>
    @endif
    {{ $treatments->withQueryString()->links() }}
@endsection