<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('appointments');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('appointments', 'AppointmentController')->middleware('auth');
Route::get('appointments/changestatus/{aid}/{sid}', 'AppointmentController@changeStatus')->name('appointments.changestatus')->middleware('auth');
Route::delete('appointments-bulk-delete', 'AppointmentController@deleteAll');
Route::get('appointments/delete/{id}', 'AppointmentController@destroy')->name('appointment.delete');

Route::resource('treatments', 'TreatmentController')->middleware('auth');
Route::delete('treatments-bulk-delete', 'TreatmentController@deleteAll');