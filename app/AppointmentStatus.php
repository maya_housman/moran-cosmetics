<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AppointmentStatus extends Model
{
    public function appointments(){
        return $this->hasMany('App\Appointment');
    }

    public static function next($status_id){
        $nextstages = DB::table('nextstages')->where('from', $status_id)->pluck('to');
        return self::find($nextstages)->all();
    }

    public static function allowed($from,$to){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;
    }
}
