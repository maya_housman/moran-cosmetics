<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Treatment extends Model
{
    use Sortable;

    protected $fillable = ['name', 'cost', 'duration'];

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }
}
