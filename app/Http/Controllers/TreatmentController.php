<?php

namespace App\Http\Controllers;

use App\Treatment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TreatmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $treatmentsQuery = Treatment::query();
        if ($sortBy = $request->input('sort')) {
            if ($request->input('direction')) {
                $treatmentsQuery->orderBy($sortBy, $request->input('direction'));
            } else {
                $treatmentsQuery->orderBy($sortBy);
            }
        }
        $treatments = $treatmentsQuery->paginate(10);
        return view('treatments.index', compact('treatments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('treatments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $treatment = new Treatment($request->all());
        $treatment->save();
        return redirect('treatments');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $treatment = Treatment::findOrFail($id);
        return view('treatments.edit', compact('treatment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $treatment = Treatment::findOrFail($id);
        $treatment->update($request->all());
        return redirect('treatments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $treatment = Treatment::findOrFail($id);
        $appointments = $treatment->appointments;
        foreach ($appointments as $appointment){
            $appointment->delete();
        }
        $treatment->delete();
        return redirect('treatments');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        $treatments = Treatment::find(explode(",", $ids));
        foreach ($treatments as $treatment) {
            $appointments = $treatment->appointments;
            foreach ($appointments as $appointment){
                $appointment->delete();
            }
            $treatment->delete();
        }
        return response()->json(['success' => "Treatments Deleted successfully."]);
    }

}
