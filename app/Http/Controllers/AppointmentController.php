<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\AppointmentStatus;
use App\PaymentStatus;
use App\Role;
use App\Treatment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (auth()->user()->role->name === 'admin') {
            $appointmentsQuery = Appointment::query();
        } else {
            $appointmentsQuery = Appointment::where('user_id', auth()->id());
        }
        if ($sortBy = $request->input('sort')) {
            if ($request->input('direction')) {
                $appointmentsQuery->orderBy($sortBy, $request->input('direction'));
            } else {
                $appointmentsQuery->orderBy($sortBy);
            }
        }

        $appointments = $appointmentsQuery->paginate(10);
        return view('appointments.index', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $users = User::where('role_id', Role::where('name', 'customer')->first()->id)->get();
        $treatments = Treatment::all();
        return view('appointments.create', compact('users', 'treatments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $appointment = new Appointment($request->all());
        if (auth()->user()->role->name === 'customer') {
            $appointment->user_id = auth()->id();
        }
        $appointment->start_time = Carbon::make($request->input('start_time'))->toDateTime();
        $appointment->appointment_status_id = AppointmentStatus::where('name', 'pending')->first()->id;
        $appointment->payment_status_id = PaymentStatus::where('name', 'Not paid')->first()->id;
        $appointment->save();
        return redirect('appointments');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $appointment = Appointment::findOrFail($id);
        $treatments = Treatment::all();
        $payment_statuses = PaymentStatus::all();
        $appointment_statuses = AppointmentStatus::all();
        $users = User::where('role_id', Role::where('name', 'customer')->first()->id)->get();
        return view('appointments.edit', compact('appointment', 'users', 'treatments', 'appointment_statuses', 'payment_statuses'));
    }

    public function changeStatus($aid, $sid)
    {
        $appointment = Appointment::findOrFail($aid);
        if(Gate::allows('change-status', $appointment))
        {
            $from = $appointment->appointmentStatus->id;
            if(!AppointmentStatus::allowed($from,$sid)) return redirect('appointments');
            $appointment->appointment_status_id = $sid;
            $appointment->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the status of the appointment because you are not admin');
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $appointment = Appointment::findOrFail($id);
        if (auth()->user()->role->name === 'admin') {
            $appointment->user_id = $request->input('user_id');
            $appointment->appointment_status_id = $request->input('appointment_status_id');
            $appointment->payment_status_id = $request->input('payment_status_id');
        } else {
            $appointment->appointment_status_id = AppointmentStatus::where('name', 'pending')->first()->id;
        }
        $appointment->treatment_id = $request->input('treatment_id');
        $appointment->start_time = Carbon::make($request->input('start_time'))->toDateTime();


        $appointment->save();
        return redirect('appointments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $appointment = Appointment::findOrFail($id);
        $appointment->delete();
        return redirect('appointments');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("appointments")->whereIn('id', explode(",", $ids))->delete();
        return response()->json(['success' => "Appointments Deleted successfully."]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validate['start_time'] = Carbon::make($data['start_time'])->hour;
        $validate['start_after_day'] = Carbon::make($data['start_time']) > Carbon::now()->addDay();
        $validate['round_hour'] = Carbon::make($data['start_time'])->minute;
        $validate['start_day'] = !Carbon::make($data['start_time'])->isSaturday() && !Carbon::make($data['start_time'])->isFriday();
        $validate['appointment_collisions'] = Appointment::where('start_time', '=', Carbon::make($data['start_time']))->count();

        $rules = [
            'start_time' => ['required', 'numeric', 'between:8,17'],
            'round_hour' => ['required', 'in:0'],
            'start_day' => ['required', 'accepted'],
            'appointment_collisions' => ['required', 'in:0'],
        ];

        $messages = [
            'start_time.between' => "The start time should be between 8AM and 17PM",
            'round_hour.in' => "The appointment time must be a round hour",
            'start_day.accepted' => "The start day should be between Sunday and Thursday",
            'appointment_collisions.in' => "The appointment time is not free",
        ];

        if (auth()->user()->role->name === 'user') {
            $rules['start_after_day'] = ['required', 'accepted'];
            $messages['start_after_day.accepted'] = 'The start time should be in more then 24 hours';
        }

        return Validator::make($validate, $rules, $messages);
    }
}
