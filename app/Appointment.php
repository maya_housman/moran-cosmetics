<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Kyslik\ColumnSortable\Sortable;

class Appointment extends Model
{
    use Sortable;

    protected $fillable = ['user_id', 'treatment_id', 'appointment_status_id', 'payment_status_id', 'start_time'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function treatment(){
        return $this->belongsTo('App\Treatment');
    }

    public function appointmentStatus(){
        return $this->belongsTo('App\AppointmentStatus');
    }

    public function paymentStatus(){
        return $this->belongsTo('App\PaymentStatus');
    }

    public function getDateAttribute($value)
    {
        return Carbon::make($this->start_time)->toDateString();
    }

    public function getTimeAttribute($value)
    {
        return Carbon::make($this->start_time)->toTimeString();
    }
}
