<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Appointment;
use App\AppointmentStatus;
use App\PaymentStatus;
use App\Role;
use App\Treatment;
use Faker\Generator as Faker;

$factory->define(Appointment::class, function (Faker $faker) {
    $customers = Role::where('name', 'customer')->first()->users;
    return [
        'user_id' => $faker->randomElement($customers)->id,
        'treatment_id' => $faker->randomElement(Treatment::all())->id,
        'appointment_status_id' => $faker->randomElement(AppointmentStatus::all())->id,
        'payment_status_id' => $faker->randomElement(PaymentStatus::all())->id,
        'start_time' => $faker->dateTime(),
    ];
});
