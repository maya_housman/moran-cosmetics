<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PaymentStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_statuses')->insert([
            [
                'name' => 'Not paid',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Paid in pepper',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], 
            [
                'name' => 'Paid in Bit',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Paid in Cash',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                                
            ]); 
    }
}
