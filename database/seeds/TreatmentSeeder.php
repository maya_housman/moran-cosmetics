<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TreatmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('treatments')->insert([
            [
                'name' => 'manicure',
                'cost'=> 120,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'pedicure',
                'cost'=> 100,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'face care',
                'cost'=> 400,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                       [
                'name' => 'Body wax',
                'cost'=> 250,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            [
                'name' => 'Body laser',
                'cost'=> 350,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                       [
                'name' => 'Face epilation',
                'cost'=> 270,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                  
            [
                'name' => 'Pigmentation treatment',
                'cost'=> 400,
                'duration' => 60,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                                            
            ]);
    }
}
