<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PaymentStatusesSeeder::class);
         $this->call(RolesSeeder::class);
         $this->call(AppointmentStatusesSeeder::class);
         $this->call(TreatmentSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(AppointmentSeeder::class);
         $this->call(NextStagesSeeder::class);
    }
}
