<?php

use App\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'moran',
                'email' => 'moran@cosmetics.com',
                'password' => Hash::make('12345678'),
                'phone' => '0507854875',
                'role_id' => Role::where('name', 'admin')->first()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'maya housman',
                'email' => 'maya@gmail.com',
                'password' => Hash::make('12345678'),
                'phone' => '0532865882',
                'role_id' => Role::where('name', 'customer')->first()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'shiran perelman',
                'email' => 'shiran@gmail.com',
                'password' => Hash::make('12345678'),
                'phone' => '0546720667',
                'role_id' => Role::where('name', 'customer')->first()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
